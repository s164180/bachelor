import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
import NNA
import scikitplot as skplt
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from numpy import genfromtxt

data = np.genfromtxt('diabetes.csv', dtype=float, delimiter=',')
X = data[1:,0:8]
y = data[1:,8]
indices = np.random.permutation(len(y))
X = X[indices,:]
y = y[indices] + 0


#Normalize X
X = (X-np.mean(X,axis=0))/np.sqrt(np.var(X,axis=0))

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=0)