import sys
import numpy as np
from sklearn.model_selection import train_test_split
import NN
import plots
import numpy.random as npr

print(sys.argv)

#Parkinson data
data = np.genfromtxt('diabetes.csv', dtype=float, delimiter=',')

y = data[1:,8]
X = data[1:,0:8]

seed = np.int(sys.argv[6])
npr.seed(seed)

indices = np.random.permutation(len(y))
X = X[indices,:]
y = y[indices] + 0


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=2)

#### Sysarg is: Tsamples B M1 Folder1 Folder2 #######

## Lets use the neural-network
Tsamples = np.int(sys.argv[1])
B = np.float(sys.argv[2])
M1 = np.int(sys.argv[3])

test = NN.NeuralPGA(X_train, y_train, M1, B, X_test, y_test, Tsamples=Tsamples)
test.MCMC()
path = '%s/%s/' %(sys.argv[4],sys.argv[5])

plots.plot_stats(test,path = path,sub_folder=sys.argv[5],folder=sys.argv[4])
