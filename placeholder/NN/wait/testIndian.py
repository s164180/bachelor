import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
import NN
import NN2
import scikitplot as skplt
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from numpy import genfromtxt

data = np.genfromtxt('heart.csv', dtype=float, delimiter=',')
X = data[1:,0:13]
y = data[1:,13]
indices = np.random.permutation(len(y))
X = X[indices,:]
y = y[indices] + 0

#Normalize X
X = (X-np.mean(X,axis=0))/np.sqrt(np.var(X,axis=0))
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=0)


#Neural network
M1 = 4
Tsamples = 1000
test = NN.NeuralPG(X_train,y_train,M1,X_test,y_test,Tsamples = Tsamples)
test.MCMC()

yhat = np.array([1 - test.pred.T, test.pred.T])
skplt.metrics.plot_roc(y_test, yhat.T)
plt.show()

test2 = NN2.NeuralPG(X_train,y_train,M1,X_test,y_test,Tsamples = Tsamples)
test2.MCMC()