import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
import NNA
from sklearn.model_selection import train_test_split

n = 100


X1 = (npr.rand(n) > 0.5) + 0
X2 = (npr.rand(n) > 0.5) + 0

y = ((X1 + X2) % 2)

X = np.array([X1,X2])

X = X.T


#Lets train on this and plot the training results
M1 = 4
Tsamples = 1000

#Shuffle data
indices = np.random.permutation(len(y))
X = X[indices,:]
y = y[indices] + 0


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=0)
plt.scatter(X[:,0],X[:,1],c=y)
plt.colorbar()
plt.show()

#Neural network
test = NNA.NeuralPG(X_train,y_train,M1,X_test,y_test,Tsamples = Tsamples)
test.MCMC()