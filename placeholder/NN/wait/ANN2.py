# Qahir Yousefi

# Import packages
import numpy as np
from pypolyagamma import PyPolyaGamma
import pypolyagamma
import numpy.random as npr
import scipy.stats as stats


class NeuralPG():
    def __init__(self, Xtrain , ytrain, M1, B, X_test = None,y_test = None, Tsamples = 1000, ppc = False, lpcheck = False):
        self.M1 = M1
        self.ntrain = len(ytrain)
        self.ntest = len(y_test)
        self.ppc = ppc
        self.Tsamples = Tsamples
        self.lpcheck = lpcheck
        self.theta = 0
        self.Xtrain = Xtrain
        self.X = Xtrain
        self.y = ytrain
        self.ytest = y_test
        self.B = B

        dim = Xtrain.shape[1]
        Beta1 = np.zeros((M1, dim))

        #Setup test data
        if X_test is not None:
            self.X_test = X_test
            self.ntest = X_test.shape[0]
            self.y2test = npr.binomial(1,1/2,self.ntest)
            self.pred = np.ones(self.ntest) * 1/2
            if (y_test is not None):
                self.y_test = y_test
            self.X = np.concatenate((self.Xtrain,self.X_test))
            self.y = np.concatenate((self.y,self.y2test))
        self.y1 = np.zeros(((self.ntrain+self.ntest),M1))

        #Initialize hidden layers
        b1 = np.zeros(dim)
        B1 = B * np.diag(np.ones(dim))
        for k in range(M1):
            Beta1[k, :] = npr.multivariate_normal(b1, B1)
            p1 = 1 / (1 + np.e ** (-self.X @ Beta1[k, :]))
            self.y1[:, k] = npr.binomial(1, p1)


        # Make layers for training
        self.layer1 = [LPG(self.X, self.y1[:, j],Tsamples = Tsamples,B=B) for j in range(self.M1)]
        y_tmp = np.zeros(self.y1.shape)
        for k in range(M1):
            y_tmp[:,k] = self.layer1[k].y
        self.layer2 = [LPG(y_tmp,self.y,Tsamples = Tsamples,B=B)]

    def logp(self):
        lp = 0
        for k in range(self.M1):
            lp += self.layer1[k].logp()
        lp += self.layer2[0].logp()
        return lp

    def MCMC(self):

        # Sample w for likelihood function of test data
        for i in range(self.Tsamples):
            # Sample in each layer
            [self.layer1[j].MCMC() for j in range(self.M1)]
            self.layer2[0].MCMC()

            for j in range(self.M1):
                ### Efficient sampling
                kappa2 = self.layer2[0].y - 1/2

                #Parts related to layer1
                phi = self.layer1[j].X @ self.layer1[j].beta
                l1 = 1/2 * phi
                l0 = -1/2 * phi

                #Parts related to layer2
                phi0 = self.layer2[0].X @ self.layer2[0].beta - self.layer2[0].X[:,j] * self.layer2[0].beta[j]
                phi1 = phi0 + self.layer2[0].beta[j]
                l0 += kappa2 * phi0 - self.layer2[0].w * (phi0**2)/2
                l1 += kappa2 * phi1 - self.layer2[0].w * (phi1**2)/2
                theta = np.exp(l1) / (np.exp(l1) + np.exp(l0))
                y_star = npr.rand(len(self.layer2[0].y)) < theta

                # Check
                if(self.lpcheck):
                    for i in range(len(self.layer1[j].y)):
                        self.layer1[j].y[i] = 0
                        self.layer2[0].X[i,j] = 0
                        lp0 = self.logp()
                        self.layer1[j].y[i] = 1
                        self.layer2[0].X[i,j] = 1
                        lp1 = self.logp()
                        thetas = np.exp(lp1)/(np.exp(lp1)+np.exp(lp0))
                        y_s = npr.rand() < thetas
                        self.layer1[j].y[i] = y_s
                        self.layer2[0].X[i,j] = y_s
                        y_star[i] = y_s
                        print(np.abs(theta[i]-thetas))

                self.layer1[j].y = y_star
                self.layer2[0].X[:,j] = y_star

            # Perform check of prior distribution
            if self.ppc:
                theta2 = 1 / (1 + np.e ** (-self.layer2[0].X @ self.layer2[0].beta))
                self.layer2[0].y = npr.binomial(1, theta2)

            # Save theta for predictions on data and update outer layer
            psi = self.layer2[0].X @ self.layer2[0].beta
            theta = 1 / ( 1+ np.exp(-psi))
            y2 = npr.binomial(1,theta[self.ntrain:])
            self.layer2[0].y[self.ntrain:] = y2

            if (i >= self.Tsamples / 2):
                self.theta += theta
                self.pred += theta[self.ntrain:]

        self.theta = self.theta[0:self.ntrain] / (self.Tsamples/2)
        self.pred = self.pred / (self.Tsamples/2)

class NeuralPGA():
    def __init__(self, Xtrain , ytrain, M1, B, X_test = None,y_test = None, Tsamples = 1000, ppc = False, lpcheck = False):
        self.M1 = M1
        self.ntrain = len(ytrain)
        self.ntest = len(y_test)
        self.ppc = ppc
        self.Tsamples = Tsamples
        self.lpcheck = lpcheck
        self.theta = 0
        self.Xtrain = Xtrain
        self.X = Xtrain
        self.y = ytrain
        self.y_test = y_test
        self.X_test = X_test
        self.B = B

        dim = Xtrain.shape[1]
        Beta1 = np.zeros((M1, dim))
        self.pred = np.zeros(self.ntest)
        self.y1 = np.zeros((self.ntrain,M1))

        #Initialize hidden layers
        b1 = np.zeros(dim)
        B1 = B * np.diag(np.ones(dim))
        for k in range(M1):
            Beta1[k, :] = npr.multivariate_normal(b1, B1)
            p1 = 1 / (1 + np.e ** (-self.X @ Beta1[k, :]))
            self.y1[:, k] = npr.binomial(1, p1)


        # Make layers for training
        self.layer1 = [LPG(self.X, self.y1[:, j],Tsamples = Tsamples,B=self.B) for j in range(self.M1)]
        y_tmp = np.zeros(self.y1.shape)
        for k in range(M1):
            y_tmp[:,k] = self.layer1[k].y
        self.layer2 = [LPG(y_tmp,self.y,Tsamples = Tsamples,B=self.B)]

        self.layer2test = np.zeros((self.ntest,M1))


    def logp(self):
        lp = 0
        for k in range(self.M1):
            lp += self.layer1[k].logp()
        lp += self.layer2[0].logp()
        return lp

    def MCMC(self):
        # Sample w for likelihood function of test data
        for i in range(self.Tsamples):
            # Sample in each layer
            [self.layer1[j].MCMC() for j in range(self.M1)]
            self.layer2[0].MCMC()

            for j in range(self.M1):
                ### Efficient sampling
                kappa2 = self.layer2[0].y - 1/2

                #Parts related to layer1
                phi = self.layer1[j].X @ self.layer1[j].beta
                l1 = 1/2 * phi
                l0 = -1/2 * phi

                #Parts related to layer2
                phi0 = self.layer2[0].X @ self.layer2[0].beta - self.layer2[0].X[:,j] * self.layer2[0].beta[j]
                phi1 = phi0 + self.layer2[0].beta[j]
                l0 += kappa2 * phi0 - self.layer2[0].w * (phi0**2)/2
                l1 += kappa2 * phi1 - self.layer2[0].w * (phi1**2)/2
                theta = np.exp(l1) / (np.exp(l1) + np.exp(l0))
                y_star = npr.rand(len(self.layer2[0].y)) < theta

                # Check
                if(self.lpcheck):
                    for i in range(len(self.layer1[j].y)):
                        self.layer1[j].y[i] = 0
                        self.layer2[0].X[i,j] = 0
                        lp0 = self.logp()
                        self.layer1[j].y[i] = 1
                        self.layer2[0].X[i,j] = 1
                        lp1 = self.logp()
                        thetas = np.exp(lp1)/(np.exp(lp1)+np.exp(lp0))
                        y_s = npr.rand() < thetas
                        self.layer1[j].y[i] = y_s
                        self.layer2[0].X[i,j] = y_s
                        y_star[i] = y_s
                        print(np.abs(theta[i]-thetas))

                self.layer1[j].y = y_star
                self.layer2[0].X[:,j] = y_star


            #Make predictions
            if (i >= self.Tsamples / 2):
                for j in range(self.M1):
                    self.layer2test[:,j] = 1 / ( 1 + np.exp(-self.X_test @ self.layer1[j].beta))
                theta = 1 / (1 + np.exp(-self.layer2test @ self.layer2[0].beta))
                self.pred += theta
            # Perform check of prior distribution
            if self.ppc:
                theta2 = 1 / (1 + np.e ** (-self.layer2[0].X @ self.layer2[0].beta))
                self.layer2[0].y = npr.binomial(1, theta2)
        self.pred = self.pred / (self.Tsamples/2)

class LPG():
    def __init__(self,X,y, Tsamples = 1000, n_sample_iter = 10, b = None, B = None, X_test = None, y_test = None):
        self.X = X
        self.y = y
        self.X_test = X_test
        self.y_test = y_test

        self.betait = 50

        self.dim = X.shape[1]
        self.lambdas = np.zeros(Tsamples//self.betait)
        self.betas = np.zeros((self.dim,Tsamples//self.betait))
        self.iter = 0
        self.n_sample_iter = n_sample_iter
        self.pg = PyPolyaGamma()
        self.w = np.zeros(len(self.y))
        self.Tsamples = Tsamples

        if b == None:
            self.b = np.zeros(self.dim)

        ''' 
        if B == None:
            self.B = 1.0 * np.diag(np.ones(self.dim))
        else:
            self.B = B * np.diag(np.ones(self.dim))
        '''

        self.a_0 = B
        self.b_0 = 1

        self.lam = stats.gamma.rvs(self.a_0,scale=1/self.b_0)
        self.B = (1/self.lam) * np.eye(self.dim)
        self.beta = npr.multivariate_normal(self.b, self.B)

    def MCMC(self):
        # Initialize
        #Binv = np.linalg.inv(self.B)
        kappa = self.y - 1/2.0
        n = len(self.y)
        beta = self.beta
        w = self.w
        lam = self.lam
        Binv = np.eye(self.dim) * self.lam

        # Gibbs sample
        for i in range(self.n_sample_iter):

            #Sample w
            w = np.array([self.pg.pgdraw(1, self.X[j,:].T @ beta) for j in range(0, n)])
            #sample beta
            OMEGA = np.diagflat(w)
            Vw = np.linalg.inv(self.X.T @ OMEGA @ self.X + Binv)
            mw = Vw @ (self.X.T @ kappa + Binv @ self.b)
            beta = npr.multivariate_normal(mw, Vw)

            #Sample lambda
            lam = stats.gamma.rvs(self.a_0,scale=1/(self.b_0 + beta.T @ beta))
            Binv = np.eye(self.dim) * lam
        self.beta = beta
        self.w = w
        if self.iter % self.betait == 0:
            self.betas[:,self.iter // self.betait] = beta
            self.lambdas[self.iter // self.betait] = lam
        self.iter += 1

    def logp(self):
        n = len(self.y)
        kappa = self.y - 1/2
        psi = self.X @ self.beta

        priorterm = np.log(stats.multivariate_normal.pdf(self.beta,mean=self.b,cov=self.B)) - n*np.log(2)
        wterm = np.sum(np.array([pypolyagamma.utils.pgpdf(self.w[j],1,0) - self.w[j]*psi[j]**2 /2.0 for j in range(0,n)]))
        kappaterm = kappa @ (self.X @ self.beta)
        lp = wterm + kappaterm + priorterm
        return(lp)
