import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
import NN
import scikitplot as skplt
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression

# Lets make square with 4 quadrants and plot it
q1 = np.array([0.5,0.5])
q2 = np.array([-0.5,0.5])
q3 = np.array([-0.5,-0.5])
q4 = np.array([0.5,-0.5])
I = np.diag([1,1])*0.02
npoints = 100
point1 = npr.multivariate_normal(q1,I,npoints)
y1 = np.ones(npoints)
point2 = npr.multivariate_normal(q2,I,npoints)
y2 = np.zeros(npoints)
point3 = npr.multivariate_normal(q3,I,npoints)
y3 = np.ones(npoints)
point4 = npr.multivariate_normal(q4,I,npoints)
y4 = np.zeros(npoints)
points = np.concatenate((point1,point2,point3,point4))
y = np.concatenate([y1,y2,y3,y4])

#Lets train on this and plot the training results
M1 = 6
Tsamples = 3000

#Shuffle data
indices = np.random.permutation(len(y))
X = points[indices,:]
y = y[indices] + 0


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=0)

plt.scatter(X[:,0],X[:,1],c=y)
plt.colorbar()
#plt.savefig('square.png')
plt.show()


#Neural network
test = NN.NeuralPG(X_train,y_train,M1,X_test,y_test,Tsamples = Tsamples)
test.MCMC()
plt.scatter(X_test[:,0],X_test[:,1],c=test.pred)
plt.colorbar()
#plt.savefig('NNsquare.png')
plt.show()

plt.scatter(X_train[:,0],X_train[:,1],c=test.theta)
plt.colorbar()
plt.show()


#ROC curve
yhat = np.array([1 - test.pred.T, test.pred.T])
skplt.metrics.plot_roc(y_test, yhat.T)
#plt.savefig('NNsquareROC.png')
plt.show()

''' 
#Logistic regression
logreg = LogisticRegression(solver='lbfgs').fit(X_train,y_train)
coef = logreg.coef_
slope = logreg.intercept_
psi = X_test @ coef.T + slope
p = 1 / (1 + np.exp(-psi))
plt.scatter(X_test[:,0],X_test[:,1],c=p[:,0])
plt.colorbar()
#plt.savefig('logisticSquare.png')
plt.show()

# Roc curve
yhat = np.array([1 - p, p])
skplt.metrics.plot_roc(y_test, yhat[:,:,0].T)
#plt.savefig('logisticSquareROC.png')
plt.show()
'''
