# Qahir Yousefi

# Import packages
import numpy as np
from pypolyagamma import PyPolyaGamma
import numpy.random as npr
import matplotlib.pyplot as plt
import scipy.stats as stats
import scikitplot as skplt
import pypolyagamma
from scipy.stats import multinomial

from sklearn import datasets
from sklearn.model_selection import train_test_split

'''
X = np.loadtxt('glass.txt',delimiter=',')
y = X[:,10] - 1
y[163:] = y[163:] - 1
X = X[:,1:10]
'''

# Shuffle data

iris = datasets.load_iris()

#Nontrivial IRIS
X = iris.data
y = iris.target

n = len(y)

npr.seed(1)

'''
seed = 1
indices = np.random.permutation(len(y))
X = X[indices,:]
y = y[indices] + 0
'''

t = np.equal.outer(y,range(3)).astype(int)
X = (X-X.mean(axis=0)) / np.sqrt(X.var(axis=0))


class NeuralPG():
    def __init__(self, X_train , y_train, M1, X_test = None,y_test = None, Tsamples = 1000, lpcheck = False):
        self.M1 = M1
        self.ntrain = X_train.shape[0]
        self.ntest = X_test.shape[0]
        self.Tsamples = Tsamples
        self.lpcheck = lpcheck
        self.theta = 0
        self.Xtrain = X_train
        self.ytrain = y_train
        self.y_test = y_test
        self.X_test = X_test
        self.J = y_train.shape[1] # Number of categories
        self.pred = np.zeros(y_test.shape)
        probas = np.ones(self.J) * 1/self.J

        #Initialize hidden layers
        # Setup test data
        self.y2test = np.random.multinomial(1,probas,size=self.ntest)
        self.X = np.concatenate((self.Xtrain, self.X_test))
        self.y = np.concatenate((self.ytrain, self.y2test))
        y1 = multinomial.rvs(1,probas,size=self.X[0])

        # Make layers for training
        self.layer1 = [LPG(self.X,y1,Tsamples = Tsamples) for j in range(self.M1)]
        X2 = np.tile(y1,self.M1)
        self.layer2 = [LPG(X2,self.y,Tsamples = Tsamples)]

    def MCMC(self):
        # Sample w for likelihood function of test data
        for i in range(self.Tsamples):
            # Sample in each layer
            [self.layer1[j].MCMC() for j in range(self.M1)]
            self.layer2[0].MCMC()

            for q in range(self.M1):
                beta = self.layer2[0].beta[q:(q + self.J), :]
                for j in range(self.J):

                    ### "Efficient" sampling

                    #Parts related to first layer
                    betaJ = np.delete(self.layer1[q].beta, j, axis=1)
                    prod = self.layer1[q].X @ betaJ
                    maxprod = np.max(prod, axis=1)
                    Cj = np.log(np.sum(np.exp(prod.T - maxprod), axis=0))
                    eta = X @ self.layer1[q].beta[:, j] - Cj
                    l1 = 1/2 * eta
                    l0 = -1/2 * eta

                    #Parts related to layer2
                    X = self.layer1[q].y
                    bJ = beta[:,j]

                    eta = X @ bJ - Cj



                    theta = np.exp(l1) / (np.exp(l1) + np.exp(l0))
                    y_star = npr.rand(len(self.layer2[0].y)) < theta

                    self.layer1[j].y = y_star
                    self.layer2[0].X[:,j] = y_star

class LPG():
    def __init__(self,X,y, Tsamples = 1000, n_sample_iter = 10, b = None):

        # Initialize
        self.X = X
        self.y = y
        self.n = np.sum(y, 1)  # number of responses of each observation
        self.betait = 50 # Beta iteration to save
        self.N = X.shape[0]  # number of observations
        self.dim = X.shape[1]  # dimension of input
        self.J = y.shape[1]  # categories
        self.n = np.sum(y, 1)  # number of responses of each observation
        self.a_0 = 1
        self.b_0 = 1
        self.n_sample_iter = n_sample_iter
        self.iter = 0
        self.betas = np.zeros((self.dim,Tsamples//self.betait)) # Save one beta to check convergence
        self.lambdas = np.zeros(Tsamples//self.betait) # Save one lambda to check convergence
        self.Tsamples = Tsamples
        self.pg = PyPolyaGamma()
        self.w = np.zeros(len(self.y))
        self.beta = np.zeros((self.dim, self.J))  # dimension x category
        self.kappa = self.y.T - self.n / 2
        self.kappa = self.kappa.T  # number of observations x categories
        self.lam = np.array([stats.gamma.rvs(self.a_0, scale=self.b_0) for j in range(self.J)])
        self.I = np.diag(np.ones(self.dim))  # dim x dim

        if b is None:
            self.b = np.zeros(self.dim)

        for j in range(self.J):
            self.beta[:, j] = npr.multivariate_normal(self.b, self.I * 1 / self.lam[j])  # 1/lams[j])

    def MCMC(self):
        # Initialize
        self.kappa = self.y.T - self.n / 2
        self.kappa = self.kappa.T
        beta = self.beta
        w = self.w
        lam = self.lam
        Binv = self.I * self.lam

        # Gibbs sample
        for q in range(self.n_sample_iter):
            for j in range(self.J):

                #Sample w
                betaJ = np.delete(self.beta, j, axis=1)
                prod = self.X @ betaJ
                maxprod = np.max(prod, axis=1)
                Cj = np.log(np.sum(np.exp(prod.T - maxprod), axis=0))

                self.eta = self.X @ self.beta[:, j] - Cj
                Binv = self.I * self.lam[j]

                # Sample w
                w = np.array([self.pg.pgdraw(self.n[k], self.eta[k]) for k in range(self.N)])

                # Sample beta
                OMEGA = np.diagflat(w)
                Vw = np.linalg.inv(self.X.T @ OMEGA @ self.X + Binv)
                mw = Vw @ (self.X.T @ (self.kappa[:, j] - OMEGA @ Cj) + Binv @ self.b)
                self.beta[:, j] = npr.multivariate_normal(mw, Vw)

                # Sample lambda
                self.lam[j] = stats.gamma.rvs(self.a_0, scale= 1/(self.b_0 + self.beta[:, j].T @ self.beta[:, j]))


        self.beta = beta
        self.w = w
        if self.iter % self.betait == 0:
            self.betas[:,self.iter // self.betait] = self.beta[:,0]
            self.lambdas[self.iter // self.betait] = self.lam[0]
        self.iter += 1
