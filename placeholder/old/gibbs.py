
#Qahir Yousefi

#Import packages

from pypolyagamma import PyPolyaGamma
import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
import scipy.stats as stats

#Initialise set seed
np.random.seed(7)

#Generate dataset
n_data = 20
beta_true = npr.normal(0,1) # 1.690525703800356
X = 2*(npr.random(n_data) - 1/2)
p = 1/(1+np.e**(-X*beta_true))
y = npr.binomial(1,p)

#Set params
Tsamples = 50000
b = 0
sigma_B = np.sqrt(3)
ppc = False
n_sample_iter = 5


# Univariate Gibbs sampler
def logreg_univariate(X,y,Tsamples=1000, b = 0, sigma_B = 1, ppc = False, n_sample_iter = 5):

    #Initialize
    pg = PyPolyaGamma() # PyPolyaGamma(seed=7) if you want to set seed
    betas = np.zeros(Tsamples//n_sample_iter) # Integer division
    w = np.zeros(y.shape)
    Binv = 1/(sigma_B**2.0)
    kappa = y - 1/2.0

    #Sample from prior
    beta = npr.normal(b,sigma_B)

    #Gibbs sample
    for i in range(Tsamples):

        # Sample w
        for j in range(len(y)):
            w[j] = pg.pgdraw(1,X[j]*beta)

        #Sample beta
        OMEGA = np.diagflat(w)
        Vw = 1 / (X.T @ OMEGA @ X + Binv)
        mw = Vw * (X.T @ kappa + Binv * b)
        beta = npr.normal(mw,np.sqrt(Vw))

        #ppc check
        if ppc:
            ps = 1.0/(1 + np.e**(-X*beta))
            ys = npr.binomial(1,ps)
            kappa = ys - 1/2.0

        #Save each n_sampler iter of beta
        if (i+1) % n_sample_iter == 0:
            betas[i // n_sample_iter] = beta # integer division

    #Make histogram plot if ppc = True
    if ppc:
        xx = np.linspace(min(betas), max(betas), 200)
        yy = stats.norm.pdf(xx, loc=b, scale=sigma_B)
        plt.figure()
        plt.hist(betas, bins='auto', density=True, ec='k')
        plt.plot(xx, yy, '-r')
        plt.xlabel('beta')
        plt.ylabel('P(beta)')
        plt.title('%dK samples of pcc from prior Beta ~ N(%.2f,%.2f^2)' % (Tsamples / 1000, b, sigma_B))
        # Save figure if you want to
        #plt.savefig("univariateCheck.png")
        plt.show()

    return(betas)


#Gibbs sample

beta = logreg_univariate(X, y, Tsamples, b, sigma_B, ppc, n_sample_iter)

if not ppc:
    plt.hist(beta, bins='auto', density=True, ec='k')
    plt.xlabel('beta')
    plt.ylabel('P(beta)')
    plt.show()