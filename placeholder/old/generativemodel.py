
'''
# Initialise set seed
np.random.seed(10)
n_data = 50
dim = 4
M1 = 4
Tsamples = 10000

### Generative model
X = npr.rand(n_data,dim)
y1 = np.zeros((n_data,M1))
y2 = np.zeros((n_data,1))

b1 = np.zeros(dim)
B1 = 1.0 * np.diag(np.ones(dim))

Beta1 = np.zeros((M1,dim))

b2 = np.zeros(M1)
B2 = 1.0 * np.diag(np.ones(M1))

for k in range(M1):
    Beta1[k,:] = npr.multivariate_normal(b1,B1)
    p1 = 1 / (1 + np.e ** (-X @ Beta1[k,:]))
    y1[:,k] = npr.binomial(1,p1)

Beta2 = npr.multivariate_normal(b2,B2)
p2 = 1 / (1 + np. e ** (-y1 @ Beta2))
y2 = npr.binomial(1,p2)
'''

'''
test = NeuralPG(X, y2, M1, ppc = True,Tsamples=Tsamples)
test.MCMC()

ind = 0

beta = test.layer1[0].betas[ind,:]
B = test.layer1[0].B
b = test.layer1[0].b
xx = np.linspace(min(beta), max(beta), 200)
yy = stats.norm.pdf(xx, loc=b[ind], scale=np.sqrt(B[ind, ind]))
plt.figure()
plt.hist(beta, bins='auto', density=True, ec='k')
plt.plot(xx, yy, '-r')
plt.title('%d samples of pcc from prior Beta ~ N(%.2f,%.2f)' % (Tsamples , b[ind], B[ind, ind]))
plt.xlabel('beta')
plt.ylabel('P(beta)')
plt.show()


plt.figure()
plt.plot(beta)
plt.show()

beta = test.layer2[0].betas[ind,:]
B = test.layer2[0].B
b = test.layer2[0].b
xx = np.linspace(min(beta), max(beta), 200)
yy = stats.norm.pdf(xx, loc=b[ind], scale=np.sqrt(B[ind, ind]))
plt.figure()
plt.hist(beta, bins='auto', density=True, ec='k')
plt.plot(xx, yy, '-r')
plt.title('%d samples of pcc from prior Beta ~ N(%.2f,%.2f)' % (Tsamples , b[ind], B[ind, ind]))
plt.xlabel('beta')
plt.ylabel('P(beta)')
plt.show()

plt.figure()
plt.plot(beta)
plt.show()
'''

