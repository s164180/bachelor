import sys
import numpy as np
from sklearn.model_selection import train_test_split
import NN
import plots
import numpy.random as npr
import pandas as pd

#Cryo data
df = pd.read_excel (r'Cryotherapy.xlsx', sheet_name='CrayoDataset')
data = df.values

y = data[:,6]
Xsex = data[:,0] == 1
Xsex = Xsex.reshape(1,90)
X1 = data[:,1:4]
X2 =data[:,5:6]

X = np.concatenate((X2.T,X1.T))
X = np.concatenate((X,Xsex))
X = X.T
y = y.T

n = len(y)
#Shuffle data
seed = np.int(sys.argv[6])
npr.seed(seed)
indices = np.random.permutation(n)
X = X[indices,:]
y = y[indices] + 0

#Normalize
X = (X-X.mean(axis=0)) / np.sqrt(X.var(axis=0))

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=2)

#### Sysarg is: Tsamples B M1 Folder1 Folder2 #######
## Lets use the neural-network
Tsamples = np.int(sys.argv[1])
B = np.float(sys.argv[2])
M1 = np.int(sys.argv[3])

test = NN.NeuralPGA(X_train, y_train, M1, B, X_test, y_test, Tsamples=Tsamples)
test.MCMC()

path = '%s/%s/' %(sys.argv[4],sys.argv[5])
plots.plot_stats(test,path = path,sub_folder=sys.argv[5],folder=sys.argv[4])
