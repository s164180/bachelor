import sys

#### Sysarg is: File_name Tsamples B M1 Folder1 Folder2 Seed Method#######

Application_Name = "%s_%s.sh" %(sys.argv[5],sys.argv[6])

##Example of original
''' 
#!/bin/sh
# embedded options to qsub - start with #PBS
# -- Name of the job ---
#PBS -N Fischertest
# \u2013- specify queue --
#PBS -q hpc
# -- estimated wall clock time (execution time): hh:mm:ss --
#PBS -l walltime=36:00:00
# \u2013- number of processors/cores/nodes --
#PBS -l nodes=1:ppn=8
# \u2013- user email address --
# please uncomment the following line and put in your e-mail address,
# if you want to receive e-mail notifications on a non-default address
#PBS -M s164180@student.dtu.dk
# \u2013- mail notification \u2013-
#PBS -m abe
# -- run in the current working (submission) directory --
if test X$PBS_ENVIRONMENT = XPBS_BATCH; then cd $PBS_O_WORKDIR; fi
# here follow the commands you want to execute
module load python3
python3 testfischer.py
'''

f = open(Application_Name,"w")
f.write("#!/bin/sh\n")
f.write("#PBS -N %s\n" % (Application_Name[:-3]))
f.write("#PBS -q hpc\n")
f.write("#PBS -l walltime=24:00:00\n")
f.write("#PBS -l nodes=1:ppn=1\n")
f.write("#PBS -M s164180@student.dtu.dk\n")
f.write("#PBS -m abe\n")
f.write("if test X$PBS_ENVIRONMENT = XPBS_BATCH; then cd $PBS_O_WORKDIR; fi\n")
f.write("module load python3\n")
f.write("python3 %s %s %s %s %s %s %s" % (sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5],sys.argv[6],sys.argv[7]))
f.close()
