import numpy as np
import scikitplot as skplt
import matplotlib.pyplot as plt
import sklearn.metrics
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_auc_score
from sklearn.metrics import log_loss
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
import sys



#Makes relevant plots
def plot_stats(NN,path,sub_folder,folder):

    # Make confusion matrix
    p = NN.pred
    pred = p > 0.5
    CM = confusion_matrix(NN.y_test, pred)


    #Make roc_curve
    yhat = np.array([1 - p, p])
    skplt.metrics.plot_roc(NN.y_test, yhat.T)
    plt.savefig('%sROC.eps' %(path))
    plt.show()


    auc = roc_auc_score(NN.y_test,p)
    LL = log_loss(NN.y_test,p)

    # Plot all beta parameters
    for i in range(NN.M1):
        nbetas = NN.layer1[i].betas.shape[0]
        fig, axs = plt.subplots(nbetas,constrained_layout=True)
        for j in range(nbetas):
            axs[j].plot(NN.layer1[i].betas[j,:],'-')

        plt.savefig('%slayer1_%d.eps' % (path,i))
        #plt.show()

    #Second layer
    nbetas = NN.layer2[0].betas.shape[0]
    fig, axs = plt.subplots(nbetas,constrained_layout=True)
    for j in range(nbetas):
        axs[j].plot(NN.layer2[0].betas[j,:],'-')
    plt.savefig('%slayer2.eps' % (path))

    #Lets test lambda
    nlambdas = NN.layer2[0].lambdas.shape[0]
    plt.figure()
    #fig, axs = plt.subplots(1,constrained_layout=True)
    plt.plot(NN.layer2[0].lambdas,'-')
    plt.savefig('%s_l2.eps' % (path))

    stats = {
        "Tsamples": NN.Tsamples,
        "B": NN.B,
        "M1": NN.M1,
        "Confusion matrix (p = 0.5)": CM,
        "AUC-score": auc,
        "Log-Loss": LL,
        "Input": sys.argv
    }
    print(stats)
    print(NN.pred)

    #Save results to relevant files
    index = sub_folder[0]
    f = open("%s/LL_%s.txt" %(folder,index), "a")
    f.write(str(LL))
    f.write('\n')
    f.close()
    f = open("%s/AUC_%s.txt" %(folder,index), "a")
    f.write(str(auc))
    f.write('\n')
    f.close()

