import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
import NN
import scikitplot as skplt
from sklearn.model_selection import train_test_split
import numpy.random as npr
import plots
import sys

from sklearn.linear_model import LogisticRegression

# Lets make square with 4 quadrants and plot it
q1 = np.array([0.5,0.5])
q2 = np.array([-0.5,0.5])
q3 = np.array([-0.5,-0.5])
q4 = np.array([0.5,-0.5])
I = np.diag([1,1])*0.02
npoints = 25
point1 = npr.multivariate_normal(q1,I,npoints)
y1 = np.ones(npoints)
point2 = npr.multivariate_normal(q2,I,npoints)
y2 = np.zeros(npoints)
point3 = npr.multivariate_normal(q3,I,npoints)
y3 = np.ones(npoints)
point4 = npr.multivariate_normal(q4,I,npoints)
y4 = np.zeros(npoints)
X = np.concatenate((point1,point2,point3,point4))
y = np.concatenate([y1,y2,y3,y4])
n = len(y)





#Shuffle data
#seed = np.int(sys.argv[6])
#npr.seed(seed)
indices = np.random.permutation(n)
X = X[indices,:]
y = y[indices] + 0

plt.figure()
plt.scatter(X[:,0],X[:,1],c=y)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=2)

#### Sysarg is: Tsamples B M1 Folder1 Folder2 #######

## Lets use the neural-network
Tsamples = np.int(sys.argv[1])
B = np.float(sys.argv[2])
M1 = np.int(sys.argv[3])

test = NN.NeuralPGA(X_train, y_train, M1, B, X_test, y_test, Tsamples=Tsamples)
path = '%s/%s/' %(sys.argv[4],sys.argv[5])
plots.plot_stats(test,path = path,sub_folder=sys.argv[5],folder=sys.argv[4])

plt.figure()
plt.scatter(X[:,0],X[:,1],c=y)
plt.colorbar()
plt.savefig("%strain.eps" % path)
plt.figure()
plt.scatter(X_test[:,0],X_test[:,1],c=test.pred)
plt.colorbar()
plt.savefig("%stest.eps" % path)