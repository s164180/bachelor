# Qahir Yousefi

# Import packages
import numpy as np
from pypolyagamma import PyPolyaGamma
import numpy.random as npr
import matplotlib.pyplot as plt
import scipy.stats as stats
import scikitplot as skplt


# Multivariate Gibbs sampler
def logreg_multivariate(X, y, b = None, B = None, X_test = None, y_test = None, Tsamples=1000, ppc=False, n_sample_iter=5):

    # Initialize
    dim = X.shape[1]
    pg = PyPolyaGamma()  # PyPolyaGamma(seed=7) if you want to set seed
    betas = np.zeros((dim,Tsamples // n_sample_iter))  # Integer division
    w = np.zeros(len(y))
    kappa = y - 1 / 2.0

    if b is None:
        b = np.zeros(dim)

    if B is None:
        B = np.diag(np.ones(dim)) * 5.0

    Binv = np.linalg.inv(B)

    a0 = 1
    b0 = 1

    #set y_hat
    yhat = np.zeros(len(y_test))

    # Sample from prior
    beta = npr.multivariate_normal(b, B)
    #Gibbs sample
    for i in range(Tsamples):
        print(i)
        # Sample w
        for j in range(len(y)):
            w[j] = pg.pgdraw(1, X[j,:].T @ beta)

        OMEGA = np.diagflat(w)
        Vw = np.linalg.inv(X.T @ OMEGA @ X + Binv)
        mw = Vw @ (X.T @ kappa + Binv @ b)
        beta = npr.multivariate_normal(mw, Vw)

        # ppc check
        if ppc:
            ps = 1 / (1 + np.e ** (-X @ beta))
            ys = npr.binomial(1,ps)
            kappa = ys - 1/2.0

        # Save each n_sample iter of beta and do AUC test
        if (i + 1) % n_sample_iter == 0:
            betas[:,i // n_sample_iter] = beta

            if X_test is not None:
                probs = 1 / (1 + np.e ** (-X_test @ beta))
                yhat = yhat + probs

    yhat = yhat/(Tsamples/n_sample_iter)

    #Make AUC curve
    if X_test is not None:
        yhat = yhat / (Tsamples // n_sample_iter)
        yhat = np.array([1-yhat,yhat])
        skplt.metrics.plot_roc(y_test, yhat.T)
        plt.show()

    #Make ppc histogram plot of one betas coordinates
    if ppc:
        ind = 0
        beta = betas[ind,:]
        xx = np.linspace(min(beta), max(beta), 200)
        yy = stats.norm.pdf(xx, loc=b[ind], scale=np.sqrt(B[ind,ind]))
        plt.figure()
        plt.hist(beta, bins='auto', density=True, ec='k')
        plt.plot(xx, yy, '-r')
        plt.xlabel('beta')
        plt.ylabel('P(beta)')
        plt.title('%dK samples of pcc from prior Beta ~ N(%.2f,%.2f)' % (Tsamples / 1000, b[ind], B[ind,ind]))
        #plt.savefig("checks.png")
        plt.show()
    return(yhat)