
import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
import scikitplot as skplt
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn import datasets
from sklearn.model_selection import train_test_split
from numpy import genfromtxt
import pandas as pd
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import log_loss
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_auc_score
import scipy.stats as stats

seeds = [0,1,2,3,4,5,6,7,8,9]


fischerLL = np.array([0.35058365318601536,0.27147940154772465,0.1971251262175634,0.27050427861514453,0.28998550421058716,0.2168377589969954,0.29683135985079107,0.2533986316497563,0.22604334144811272,0.2989221744699837])
fischerAUC = np.array([0.93048128342246,0.9572192513368983,1.0,0.9554367201426024,0.975,0.9733333333333333,0.9345238095238095,0.979047619047619,0.9736842105263158,0.9384920634920635])
cryoLL = np.array([0.39263067600133816,0.3294313603510504,0.3613020746124823,0.32911361615137447,0.47867132276959495,0.2775235014781426,0.3643569368044933,0.5567942186318956,0.22455840929718054,0.36317261387879946])
cryoAUC = np.array([0.8687782805429864,0.9285714285714285,0.8794642857142857,0.9466666666666668,0.8571428571428572,0.96875,0.9665071770334928,0.8482142857142857,0.9773755656108597,0.9155555555555556])
breastAUC = np.array([0.7032085561497325,0.8222222222222223,0.8,0.7394736842105263,0.825,0.875,0.7263157894736841,0.7804232804232805,0.7671957671957672,0.8201058201058201])
breastLL = np.array([0.6431328099511534,0.5305117155722083,0.5393694380287465,0.6217043144660672,0.5142220407943119,0.48047622238969456,0.6101991834426408,0.5476082225019785,0.5876951353054601,0.584927327885192])
parkinsonsLL = np.array([0.33565034047680364,0.3029111741394051,0.31020841090685436,0.3109281602463048,0.31899320015435756,0.4200950798687956,0.31847439219208507,0.24436793995815526,0.2752426259810391,0.3220331571806967])
parkinsonsAUC = np.array([0.8903061224489796,0.8728070175438596,0.9399509803921569,0.9285714285714286,0.9325980392156863,0.9134199134199135,0.9444444444444445,0.9523809523809523,0.9528619528619529,0.9053333333333333])



AUC = np.zeros(len(seeds))
LL = np.zeros(len(seeds))

eta = len(seeds) - 1
K = len(seeds)

#Nontrivial IRIS
print('Nontrivial Fischer')
iris = datasets.load_iris()

for i in seeds:
    X = iris.data
    y = iris.target
    y = y == 1
    n = len(y)
    npr.seed(i)
    indices = np.random.permutation(n)
    X = (X - np.mean(X, axis=0)) / np.sqrt(np.var(X, axis=0))
    X = X[indices,:]
    y = y[indices] + 0
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=2)
    mlp = MLPClassifier(hidden_layer_sizes=(4,), max_iter=10000,activation='logistic')
    mlp = mlp.fit(X_train,y_train)
    pred = mlp.predict_proba(X_test)
    pred = pred[:,1]
    AUC[i] = roc_auc_score(y_test, pred)
    LL[i] = log_loss(y_test, pred)

print(AUC)
print(LL)
print("AUC: %f CI: %f: " % (np.mean(AUC),np.sqrt(np.var(AUC)/np.sqrt(len(AUC)-1))))
print("LL: %f CI: %f" % (np.mean(LL),np.sqrt(np.var(LL)/np.sqrt(len(LL)-1))))

z = np.array(fischerAUC - AUC)
nu = K - 1
zhat = z.mean()
sig = np.sqrt(np.var(z)/K)
zstat = (0 - zhat)/sig
p = stats.t.cdf(zstat,df=nu)
print(p)

z = np.array(fischerLL - LL)
nu = K - 1
zhat = z.mean()
sig = np.sqrt(np.var(z)/K)
zstat = (0 - zhat)/sig
p = 1 - stats.t.cdf(zstat,df=nu)
print(p)


AUC = np.zeros(len(seeds))
LL = np.zeros(len(seeds))


#Breast cancer IRIS
print('Breast cancer dataset')
data = np.genfromtxt('breast.csv', dtype=float, delimiter=',')
for i in seeds:
    X = data[1:, 0:9]
    y = data[1:, 9]
    y = y == 1
    n = len(y)
    npr.seed(i)
    indices = np.random.permutation(n)
    X = X[indices,:]
    y = y[indices] + 0
    X = (X - X.mean(axis=0)) / np.sqrt(X.var(axis=0))
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=2)
    mlp = MLPClassifier(hidden_layer_sizes=(4,), max_iter=10000,activation='logistic')
    mlp = mlp.fit(X_train,y_train)
    pred = mlp.predict_proba(X_test)
    pred = pred[:,1]
    AUC[i] = roc_auc_score(y_test, pred)
    LL[i] = log_loss(y_test, pred)

print(AUC)
print(LL)
print("AUC: %f CI: %f: " % (np.mean(AUC),np.sqrt(np.var(AUC)/np.sqrt(len(AUC)-1))))
print("LL: %f CI: %f" % (np.mean(LL),np.sqrt(np.var(LL)/np.sqrt(len(LL)-1))))


z = np.array(breastAUC - AUC)
nu = K - 1
zhat = z.mean()
sig = np.sqrt(np.var(z)/K)
zstat = (0 - zhat)/sig
p = stats.t.cdf(zstat,df=nu)
print(p)

z = np.array(breastLL - LL)
nu = K - 1
zhat = z.mean()
sig = np.sqrt(np.var(z)/K)
zstat = (0 - zhat)/sig
p = 1 - stats.t.cdf(zstat,df=nu)
print(p)


AUC = np.zeros(len(seeds))
LL = np.zeros(len(seeds))

#Parkinson data
print('Parkinson dataset')
data = np.genfromtxt('parkinsons.data', dtype=float, delimiter=',')

for i in seeds:
    y = data[1:, 17]
    X1 = data[1:, 1:17]
    X2 = data[1:, 19:]
    X = np.concatenate((X1.T, X2.T))
    X = X.T
    y = y.T
    # Normalize
    X = (X - np.mean(X, axis=0)) / np.sqrt(np.var(X, axis=0))

    n = len(y)
    npr.seed(i)
    indices = np.random.permutation(n)
    X = X[indices,:]
    y = y[indices] + 0
    X = (X - X.mean(axis=0)) / np.sqrt(X.var(axis=0))
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=2)
    mlp = MLPClassifier(hidden_layer_sizes=(4,), max_iter=10000,activation='logistic')
    mlp = mlp.fit(X_train,y_train)
    pred = mlp.predict_proba(X_test)
    pred = pred[:,1]
    AUC[i] = roc_auc_score(y_test, pred)
    LL[i] = log_loss(y_test, pred)

print(AUC)
print(LL)
print("AUC: %f CI: %f: " % (np.mean(AUC),np.sqrt(np.var(AUC)/np.sqrt(len(AUC)-1))))
print("LL: %f CI: %f" % (np.mean(LL),np.sqrt(np.var(LL)/np.sqrt(len(LL)-1))))


z = np.array(parkinsonsAUC - AUC)
nu = K - 1
zhat = z.mean()
sig = np.sqrt(np.var(z)/K)
zstat = (0 - zhat)/sig
p = stats.t.cdf(zstat,df=nu)
print(p)

z = np.array(parkinsonsLL - LL)
nu = K - 1
zhat = z.mean()
sig = np.sqrt(np.var(z)/K)
zstat = (0 - zhat)/sig
p = 1 - stats.t.cdf(zstat,df=nu)
print(p)

AUC = np.zeros(len(seeds))
LL = np.zeros(len(seeds))

# Cryotherapy dataset
print('Cryo dataset')
#Cryo data
df = pd.read_excel (r'Cryotherapy.xlsx', sheet_name='CrayoDataset')
data = df.values

for i in seeds:
    y = data[:, 6]
    Xsex = data[:, 0] == 1
    Xsex = Xsex.reshape(1, 90)
    X1 = data[:, 1:4]
    X2 = data[:, 5:6]

    X = np.concatenate((X2.T, X1.T))
    X = np.concatenate((X, Xsex))
    X = X.T
    y = y.T
    # Normalize
    #X = (X - np.mean(X, axis=0)) / np.sqrt(np.var(X, axis=0))

    n = len(y)
    npr.seed(i)
    indices = np.random.permutation(n)
    X = X[indices,:]
    y = y[indices] + 0
    X = (X - X.mean(axis=0)) / np.sqrt(X.var(axis=0))
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=2)
    mlp = MLPClassifier(hidden_layer_sizes=(4,), max_iter=10000,activation='logistic')
    mlp = mlp.fit(X_train,y_train)
    pred = mlp.predict_proba(X_test)
    pred = pred[:,1]
    AUC[i] = roc_auc_score(y_test, pred)
    LL[i] = log_loss(y_test, pred)

print(AUC)
print(LL)
print("AUC: %f CI: %f: " % (np.mean(AUC),np.sqrt(np.var(AUC)/np.sqrt(len(AUC)-1))))
print("LL: %f CI: %f" % (np.mean(LL),np.sqrt(np.var(LL)/np.sqrt(len(LL)-1))))


z = np.array(cryoAUC - AUC)
nu = K - 1
zhat = z.mean()
sig = np.sqrt(np.var(z)/K)
zstat = (0 - zhat)/sig
p = stats.t.cdf(zstat,df=nu)
print(p)

z = np.array(cryoLL - LL)
nu = K - 1
zhat = z.mean()
sig = np.sqrt(np.var(z)/K)
zstat = (0 - zhat)/sig
p = 1 - stats.t.cdf(zstat,df=nu)
print(p)