# Qahir Yousefi

# Import packages
import numpy as np
import numpy.random as npr
import scipy.stats as stats
from pypolyagamma import PyPolyaGamma
from sklearn import datasets
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt


'''
# Shuffle data

iris = datasets.load_iris()

#Nontrivial IRIS
X = iris.data
y = iris.target

n = len(y)

npr.seed(1)

seed = 1
indices = np.random.permutation(len(y))
X = X[indices,:]
y = y[indices] + 0
'''

''' 
X = np.loadtxt('glass.txt',delimiter=',')
y = X[:,10] - 1
y[163:] = y[163:] - 1
X = X[:,1:10]
'''

iris = datasets.load_iris()
X = iris.data
y = iris.target
#X = (X-X.mean(axis=0)) / np.sqrt(X.var(axis=0))
t = np.equal.outer(y,range(3)).astype(int)


# Multivariate Gibbs sampler
def logreg_multivariate(X, y, b = None, a_0 = 0,b_0 = 0,X_test = None, y_test = None, Tsamples=100, n_sample_iter=5,ppc = False):

    # Initialize
    N = X.shape[0] # number of observations
    dim = X.shape[1] # dimension of input
    J = y.shape[1] # categories
    n = np.sum(y,1) # number of responses of each observation
    a_0 = 1 # Prior parameter of lambda
    b_0 = 1 # Prior parameter of lambda

    pg = PyPolyaGamma()  # PyPolyaGamma(seed=7) if you want to set seed
    betas = np.zeros((dim, J)) # dimension x category
    betasloop = np.zeros((dim,J)) # Beta parameter used for looping
    kappas = y.T - n/2
    kappas = kappas.T # number of observations x categories
    pvals = np.zeros(y.shape)


    lams = np.array([stats.gamma.rvs(a_0, scale=1/b_0) for j in range(J)])

    I = np.eye(dim) # dim x dim
    betasplot = np.zeros((dim,Tsamples // n_sample_iter))# Store beta for plot

    B = 10
    ind = 0

    if b is None:
        b = np.zeros(dim)

    for j in range(J):
        #betas[:,j] = npr.multivariate_normal(b,I * B)#1/lams[j])
        betas[:,j] = npr.multivariate_normal(b,I /lams[j])

    #Gibbs sample
    for q in range(Tsamples):
        for j in range(J):
            # Initialize
            print("%d %d" % (q,j))
            betaJ = np.delete(betas,j,axis=1)
            prod = X @ betaJ

            # No numerical errors
            maxprod = np.max(prod,axis=1)
            Cj = maxprod + np.log(np.sum(np.exp(prod.T - maxprod),axis=0))

            #Version that makes numerical errors
            #Cj = np.log(np.sum(np.exp(prod),axis=1))


            eta = X @ betas[:,j] - Cj
            #Binv = I * 1/B#lams[j]
            Binv = I * 1/lams[j]

            # Sample w
            w = np.array([pg.pgdraw(n[k], eta[k]) for k in range(N)])

            # Sample beta
            OMEGA = np.diagflat(w)

            Vw = np.linalg.inv(X.T @ OMEGA @ X + Binv)
            mw = Vw @ (X.T @ (kappas[:,j] + OMEGA @ Cj) + Binv @ b)
            print(mw)

            betas[:,j] = npr.multivariate_normal(mw.T, Vw)
            #betasloop[:,j] = npr.multivariate_normal(mw.T, Vw)

            # Sample lambda
            #lams[j] = stats.gamma.rvs(a_0,scale=1/(betas[:,j].T @ betas[:,j]+b_0))
            # Save each n_sample iter of beta and do AUC test
            if (q + 1) % n_sample_iter == 0 and j == ind:
                betasplot[:, q // n_sample_iter] = betas[:,ind]
                #betasplot[:, q // n_sample_iter] = betasloop[:,0]

        #betas = betasloop

        #betas = betasloop
        if ppc:
            prod = X @ betas

            #Rewritten to avoid numerical errors
            maxprod = np.max(prod, axis=1)
            negterm = maxprod + np.log(np.sum(np.exp(prod.T - maxprod), axis=0))
            p = np.exp(prod.T - negterm)
            p = p.T

            # Version that makes numerical errors
            #p = np.exp(prod).T/np.sum(np.exp(prod),axis=1)
            #p = p.T

            for i in range(N):
                y[i,:] = np.random.multinomial(1,p[i,:],1)

            n = np.sum(y, 1)  # number of responses of each observation
            kappas = y.T - n/2.0 #1 / 2
            kappas = kappas.T  # number of observations x categories

        #Predict
        if (q + 1) % n_sample_iter == 0 and q >= Tsamples/2:
            # This is rewritten using the rule log(a+b) = log(a) + log(1 + b/a) to avoid numerical errors
            prod = X @ betas
            maxprod = np.max(prod,axis=1)
            negterm = maxprod + np.log(np.sum(np.exp(prod.T - maxprod),axis=0))
            p = np.exp(prod.T - negterm)
            p = p.T
            pvals = p + pvals

    if ppc:
        index = 0
        beta = betasplot[index,:]
        print('Mean of each beta coordinate')
        print(betasplot.mean(axis=1))
        xx = np.linspace(min(beta), max(beta), 200)
        yy = stats.norm.pdf(xx, loc=0, scale=np.sqrt(B))
        plt.figure()
        plt.hist(beta, bins='auto', density=True, ec='k')
        plt.plot(xx, yy, '-r')
        plt.xlabel('beta')
        plt.ylabel('P(beta)')
        #plt.title('%dK samples of pcc from prior Beta ~ N(%.2f,%.2f)' % (Tsamples / 1000, b, B))
        #plt.savefig("checks.png")
        plt.show()

    pvals = pvals/((Tsamples/2)/n_sample_iter)
    return(pvals)

k = t + 0
probas = logreg_multivariate(X, k, Tsamples=1000,ppc=False,n_sample_iter=5)
print('Percentage in each class')
print(np.sum(probas,axis=0))
print(np.sum(np.sum(probas,axis=0)))

# Compare to correct in each class
predict = np.argmax(probas,axis=1)
ncor = np.sum(predict == y)
