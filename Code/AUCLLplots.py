import numpy as np
import matplotlib.pyplot as plt

datasets = ['breast','fischer','cryo','parkinsons']
params = ['N','B','M']

Names = ['N','$a_0$','M']

baseline = ['ANN-breast','ANN-fischer','ANN-cryo','ANN-parkinsons']

AUC = np.zeros((len(datasets),5))
LL = np.zeros((len(datasets),5))

B = np.array(['0.1','1','2','5','10'])
M = np.array(['2','4','6','8','10'])
N = np.array(['100','500','1000','10000','50000'])

NNAUC = np.array([[0.795415,0.857163,0.889616,0.918846],]*5)
NNLL = np.array([[0.560963,0.391240,0.408096,0.370389],]*5)

MAUC = np.array([[0.825741,0.832774,0.891039,0.932737],]*5)
MLL = np.array([[0.574996,0.283400,0.305507,0.437541],]*5)

X = np.array([N.T,B.T,M.T])

plt.figure(figsize=[12,5])

for i1,i in enumerate(params):
    for j1,j in enumerate(datasets):
        for k in range(5):
            path = "%s%s" %(j,i)
            AUC_file = "AUC_%d.txt" % k
            LL_file = "LL_%d.txt" % k

            with open(("%s/%s") %(path,AUC_file)) as f:
                contents = f.read()
                contents = contents.split("\n")
                contents = contents[:-1]
                data = [float(i) for i in contents]
                mean = np.mean(data)
                AUC[j1,k] = mean
            with open(("%s/%s") %(path,LL_file)) as f:
                contents = f.read()
                contents = contents.split("\n")
                contents = contents[:-1]
                data = [float(i) for i in contents]
                mean = np.mean(data)
                LL[j1,k] = mean

    c = np.concatenate((datasets,baseline))
    #plt.figure()
    plt.subplot(2,3,i1+1)
    plt.plot(AUC.T,'-o')
    if i == 'M':
        plt.plot(MAUC,'--')
    else:
        plt.plot(NNAUC,'--')
    #plt.legend(c,loc='center left', bbox_to_anchor=(1, 0.5))

    plt.xlabel("%s" % Names[i1])
    plt.ylabel('AUC')
    #plt.title("%s - AUC" % i)
    plt.xticks(range(0,5),X[i1,:])
    if (i1 == 2):
        plt.legend(c, loc='center left', bbox_to_anchor=(1, 0.5))
    #plt.tick_params(axis='x',pad=15)
    #plt.tight_layout()
    #plt.savefig("%sAUC.eps" % i)

    plt.subplot(2,3,i1+4)
    #plt.figure()
    plt.plot(LL.T, '-o')
    if i == 'M':
        plt.plot(MLL,'--')
    else:
        plt.plot(NNLL,'--')
    #plt.legend(c,loc='center left', bbox_to_anchor=(1, 0.5))
    plt.xlabel("%s" %  Names[i1])
    plt.ylabel('LL')
    #plt.title("%s - LL" % i)
    plt.xticks(range(0,5),X[i1,:])
    #plt.tick_params(axis='x',pad=15)
    #plt.savefig("%sLL.eps" % i)

c = np.concatenate((datasets,baseline))
plt.legend(c,loc='center left', bbox_to_anchor=(1, 0.5))
plt.tight_layout()
plt.savefig('All.eps')