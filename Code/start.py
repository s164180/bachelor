import os
import sys
import numpy as np
#### Sysarg is: File_name Folder1 folds#######
File_name = sys.argv[1]
Folder1 = sys.argv[2]
folds = np.int(sys.argv[3])

#### Sysarg  inside loop is: File_name Tsamples B M1 Folder1 Folder2 Seed Method#######

#### Iterate over Tsamples
Tsamples = [100,500,1000,10000,50000]

nparams = len(Tsamples)

for i in range(nparams):
    for j in range(folds):
        settings = 'python3 init.py %s %d 1 4 %sN %d%d %d' % (File_name, Tsamples[i], Folder1, i, j, j)
        os.system(settings)
        Application_Name = "%sN_%d%d.sh" % (sys.argv[2], i, j)
        os.system("qsub %s" % (Application_Name))
        os.system("rm -rf %s" %(Application_Name))

B = [1,2,3,4,5]

for i in range(nparams):
    for j in range(folds):
        settings = 'python3 init.py %s 10000 %f 4 %sB %d%d %d' % (File_name, B[i], Folder1, i , j,j)
        os.system(settings)
        Application_Name = "%sB_%d%d.sh" % (sys.argv[2], i, j)
        os.system("qsub %s" % (Application_Name))
        os.system("rm -rf %s" % (Application_Name))

M = [2,4,6,8,10]

for i in range(nparams):
    for j in range(folds):
        settings = 'python3 init.py %s 10000 1 %d %sM %d%d %d' % (File_name, M[i], Folder1, i, j,j)
        os.system(settings)
        Application_Name = "%sM_%d%d.sh" % (sys.argv[2], i, j)
        os.system("qsub %s" % (Application_Name))
        os.system("rm -rf %s" % (Application_Name))