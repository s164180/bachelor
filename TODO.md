# Meeting 14
 - Frys seed i rng-generatoren. Find mindste problem med underflow (fx. mindste antal dimensioner eller samples)
 - Find den update-op hvor ```NaN``` opstaar. 
 - NaN's opstaar nok fordi en parametervaerdi divergerer eller lignende. Det er vigtigt at finde ud af hvordan den bliver opdateret og om det virker rimeligt ift. den maade den bliver opdateret
 - I stedet for 2-lags NN, opstaar problemet med en 1-lags NN? (i.e. multinomial regression). En "hacky" maade at ordne det paa er at fryse Y-vaerdierne i dets kjulte lag og ikke opdatere vaegtene ind i det skjulte lag (det er et simplere problem, maaske lettere at forstaa hvad der gaar galt her). 
 - Husk special case hvor vi fryser det skjulte lag, kun sampler output-vaegte, og output-dim er K=2 (2 klasser) skulle svare til log. reg. Kan vi faa den til at fejle? 
 
# Uge 13
 - ESS/dist. relevante for at sammenligne samplere. Dvs. de kunne bruges til at sammenligne med Holmes med samme datasaet. Det kan dog vaere lidt tricky at udregne autokorrelationen (brug helst en pakke). Check holmes for om de gemmer alle samples til ESS.
 - til multinomial regression ll for sample i:
 
```math
LL = -\log p(y_i^\text{test} | x_i^\text{test} ) = -\sum_{m=1}^M \log p(y_{im}^\text{test} | x_i^\text{test} )
```
hvor sandsynligheden er udregnet over hele kaeden. Udregn ogsaa accuracy (hvor ofte er modellen korrekt). 

 - Overvej at implementere modellen i pymc3 og sammenlign resultater (check eksempler i dokumentation og genbrug dem)

```python
import theano

x = np.random.randn(100) # husk at de skal vaere N x M-dimensionelle.
y = x > 0 # N x 1 dimensionel...

x_shared = theano.shared(x)
y_shared = theano.shared(y)

with pm.Model() as model:
    
    hidden = []
    hidden_coeff = []
    
    # Dvs. BETA = M x K_hidden stor (normal) distributed beta vector; log_hidden = logistic( x * BETA  ) 
    # maaske er ide at lave en meget stor vector af beta-vaerdier
    for k in range(K_hidden):
        beta_ = pm.Normal('hidden_beta_%i'%k, mu=0, sd=1)
        hidden_coeff.append(  beta_  ) 
        log_ = pm.math.sigmoid(beta_ * x_shared)
        bern_ = pm.Bernoulli('bern_hidden_%i'%k, p=log_) # virker det her overhovedet?
        hidden.append(bern_)
    
    
    # problem: hvordan tager man hidden (en liste af bernouilli-variable) og sampler dem til en matrix der er N x K_hidden stor...
    hidden_act = 0 # reshape hidden til N x K_hidden matrix af coefficienter i det skjulte lag.
    beta_out = pm.Normal('beta_out', mu=0, sd=1) # sd = sqrt(B) i vores notation, mu=b=0.
    logistic_out = pm.math.sigmoid(hidden_act * beta_out) # theta er sigmoid(x * beta)
    
    pm.Bernoulli('obs', p=logistic_out, observed=y_shared)
    trace = pm.sample()
```


# Uge 12
 - Holmes and Held 2006: Skim artiklen igennem for at faa ideer til eksperimenter. Bemærk de bruger også et aux.var. trick, så det er relevant at diskutere dem i introduktionen. En ide kunne være at implementere/sammenligne med hvad de gør.
 - Multiclass: Lav udledning af gibbs-sampler (conditional likelihood) som vi talte om (dvs. samme trick som Holmes men i vores notation). Implementer. Brug Holmes til at få ideer til tests.
 
  
# Uge 11
 - Test hvordan performance afhænger af K2 (skjulte neuroner). Dvs. for hver datasæt, foretag (for effektivitet) hold-out CV og plot AUC som funktion af K2 for K2 =2, 4, .. 10. Plot de forskellige datasæt som farvede kurver i samme plot (evt). 
 - Test hvordan performance afhænger af B = I sigma_0^2 (prior på vægtene). Samme plot som ovenfor, AUC som funktion af sigma0 for log-spaced værdier, 0.01, 0.1, 0.5, 1, 2, 10.
 - (evt.) samme som ovenfor, men AUC som funktion af kørselstid (T=500, 1000, 10'000, ...). 
 - Implementer log-loss (averaged). dvs. 1/N_test Bern(y_i | \bar{theta}_i) og inkluder også plots med denne fejl. 
 - Multi-class data (dvs. hver observation kan tilhøre fra 0 til K categorier. Google evt. "latent dirichlet allocation" (ca. 2008) for ideer til datasæt/framing

# Uge 10
 - Gennemtest problemet med prediktion vha. Metode (A) (dvs. gemte vaerdier af beta fra MCMC sampling kun paa traeningsdata; se papir). Test i tilfaeldet af smaa datasaet (hurtig konvergens). Skulle give resultater sammenligneligt med den anden metode til at prediktere. Disse to metoder skal beskrives i teorisektionen.
 - Gem nogle tusinde beta-parametre undervejs og lav traceplots for at  have en ide om konvergens.
 - Find et antal (gerne 5+) smaa dataset fra UCI. Helst ikke-trivielle (dvs. ikke AUC=1 med log reg.)
 - find ud af at bruge GBAR clusteren (commandoer, qsub, qstat, etc.; ogsaa showclass compute -u). Se evt. online dokumentation. Brug koe 'hpc' hvis compute ikke er tilgaengelig.
 - Saet clusteren op til at koere automatiske eksperimenter. fx. : Koer alle datasaet med 50'000 samples og 5 "restarts", eller variering af parametre, etc. Bemaerk at du nok skal bruge command-line arguments til dine python scripts (python3 my_experiment_01.py -dataset=1). Se: https://www.pythonforbeginners.com/system/python-sys-argv
 - Check effekt af lange koersler paa konvergens. 
 - Automatiser generering af vigtigste plots + tabeller til latex fil.
 - Check evt. om der er multi-class dataset. Dvs. dataset hvor hver observation kan tilhoere en eller flere af K classer. 
 
# Uge 9
    - Implementer kode til at prediktere på test-data (blok trænings/test, sample y_test undervejs, gem sandsynligheder for y^test_i = 1 som udregnet fra Bern fordeling, tag middelvaerdi for at faa endelig sandysnlighed. Se papir).
    - Find 3-4 simple datasæt for klassifikation som kan bruges igennem hele rapporten (hint: god ide at have mange datasæt til at begynde med, og luge lidt ud i dem undervejs hvis et af dem er dårlige. Det er snyd, men alle gør det).
    - Undersøg effekt af B (prior). Dvs. udregn trænings/test AUC/accuracy (ved ovenstående metode) for forskellige værdier af B og for de forskellige datasæt.
    - Find på et cool navn
    - Kunstigt data studie, evt. XOR problemet, for at demonstrere metoden virker ikke-trivielt (evt. nuævrende problem med de 4 blokke).
    - Rigtigt data studie: Generer tabel med resultater hvor der indgår: (1) Almindelig logistisk regression (i.e. python)  (2) vores MCMC log reg (3) evt. en tredie metode (DT, KNN, ANN) (vent evt.) (4) vores metode for et velvalgt B (fra ovenstående, behøves ikke CV)
    - inkluder sektion hvor det demonstreres ved selv-konsistent check metoden virker (dvs. eksisterende checks)
        - Introduktion
            - Vigtig: Begrænsninger ved deep learning; dvs. 
            - ikke korrekt Bayesiansk læring af parametrene (google Bayesian deep learning, meget litteratur; bla. Neal Radford som første reference)
            - Neuronerne er i den menneskelige hjerne stokastiske (deep learning er deterministisk)
            - Bayesianske metoder til Deep Learning er langsom (Hamiltonian MCMC)
            - Vi løser det ved PG tricket og ved en fuld bayesiansk model. 
        - Teori (PG tricket, log reg, ann)
        - Implementation + detaljer om sampler (dvs. de blokke vi bruger, hvilke variable opdateres, efter hvilke fordelinger)
        - Implementation --> evaluering af test-data
        - Eksperimenter: 
            - Demonstration af at sampleren virker ved tests (se ovenfor), både log reg og ANN
            - Kunstigt data studie
            - Rigtig data

# Uge 8:
    - Effektiv sampling af y'erne. Lad y(i) være y for en bestemt skjult neuron for observation i (fra 1 til N)
    - Vi sampler
```math
y(i) \sim Bern(\theta_i ), \quad \theta_i = \frac{ p(...y(i) = 1, ... ) }{p(...y(i) = 0 ) + p(...y(i) = 1)}
```
    - som koden er nu goeres det i et for-loop over i. Ide: Udregn alle paa een gang for hver neuron, sample derefter alle N y'er.
    - Implementer check: Udregn theta as above, then make a for loop where theta is computed exactly as now for each i. 
      this gives two theta vectors (one efficient and one slow). They should agree to numerical precision. Implement check for this. 
    - Try to run the code in a profiler and see if there are obvious improvements (use realistic settings for hidden layers, N, etc.)
    - Experiments: Make an artificial data experiment (i.e. construct some trivial 2d classification problem to solve)
    - Run on Fisher iris data (try one class vs. rest). 
    - Try effect of hidden layer size.
    - Fra sampleren, gem \theta^(2)_1(i) = sigmoid(y^(1)^T beta) for i=1,...,N, og for hver af de T samples.  
    - den predikterede sandsynlighed for observation i er nu bare middelbvaerdien af \theta^(2)_1(i) for hver af de T gemte samples (en god ide at goere det her efter sampleren er koert, dvs. gem data, load, udfoer de her skridt som post-processering)
    - Udregn AUC og accuracy udfra ovenstaaende paa traenings-data. Begynd at lave eksperimenter hvor T varieres (skal nok vaere ret stor), og isaer neuroner i skjulte lag. 
    - Proev ogsaa at variere B i prior for beta ~ N(0, B) (fx. B = 1, 0.1, etc.)
    - forhbaabentligt for vi en AUC der er sammenlignelig med logistisk regression. 
    - proev problemet med de 4 clusters i hjoernerne af et rektangel og to klasser som ikke kan sepereres med logistisk regression. Proev at farve 
     hver observation med theta (dvs. theta=0 er blaa og theta=1 er roed) for visuelt at se hvad NN goer. 
    

# Uge 7:
 - Udregn E[beta], std[beta]
 - Find det mindste problem (N, M) hvor der virkeligt er problemer (er det N=2? er det M=2?)
    - dvs. ved flere, lange koersler er middelvaerdi og std systematisk forkerte. 
    - God ide at skrive middelvaerdier, std. ned i fx. excel for at faa en ide om systematiske fejl).
    - hvad er problemtypen: Er E[beta] altid < 0? er std. altid for stor?
    - Brug fast X.
    - Udregn ogsaa middelvaerdi for y^1, y^2
 - Lav traceplots af beta, evt. ogsaa af y og w (hvis fejlen ikke umiddelbart findes).
 - Evt. ogsaa traceplots af std[beta] (for at se om den konvergerer eller ej).
 - Lad os sige intet virker efter et par dage. Ide: Fjern en del af modellen (dvs. y^2 og beta^2). 
 - https://theclevermachine.wordpress.com/2012/11/05/mcmc-the-gibbs-sampler/
 - https://link.springer.com/content/pdf/10.3758%2Fs13423-016-1015-8.pdf
 @book { 292916
        author	= "Christian Robert and George Casella", 
        title	= "Monte Carlo Statistical Methods" 
 }
  
# Uge 6:
- posterior predictive check: M1 = M2 = 1, og N sat meget lavt (N = 5 eller 10). Plot beta (fx. een beta fra hvert lag)
- Derefter, et "ikke-trivielt, trivielt problem", M1=M2=4, N=5 eller 10. plot beta (fx. en fra hvert lag)
- implementer "stats" datastruktur. Fx. bare en stor dikt eller lignende. Ideen er at gemme beta-vaerdierne fra hver neuron, og y-vaerdierne fra hver neuron (dvs. parametre i LGP)
 med mellemrum. Vi bruger dem til at lave forudsigelser, etc., og til PPC. 
- Når alt det der virker: test-data (men det kommer vi til..). 
- Evt.: Tænk over hvilke y-værdier der kan opdateres på een gang
- Google efter en god mcmc-reference. Ideelt noget med test-data
- dvs. udregn forskel i log-p effektivt, og for alle N observationer hver gang (men stadig kun een LGO-enhed idet de kobler)
```python
for j in range(M1):
    for i in range(N):
        # udregn (det sker i flere skridt, selvf'lgeligt)
        lp0[i,:] = p(.... y^1_{ji} = 0), p(.... y^1_{ji} = 1)]
    
    # det er faktisk her den effektive kode skal vaere. 
    
    p_y[i] = exp(lp0[i,1]) / sum(exp(lp0[i,:]) )
```
Idden er, at udregne lp effektivt. Dvs. uden at kalde logp, som kraever at vi aendre hele datastruktur. dvs.
```python
for j in range(M1):
    lp = (en hel del python-kode; se paa likelihood-funktionen og find ud af hvordan lp skal udregnes paa een gang)
    if DEBUG:
        # check at:
        assert( lp == lp0) # med usikkerhed 10^-10
    
    p_y[i] = exp(lp[i,1]) / sum(exp(lp[i,:]) )
```
mere korrekt:

```python
for j in range(M1):
    # det er faktisk her den effektive kode skal vaere. 
    lp = (en hel del python-kode; se paa likelihood-funktionen og find ud af hvordan lp skal udregnes paa een gang)
    if DEBUG:
        for i in range(N):
            # udregn (det sker i flere skridt, selvf'lgeligt)
            lp0[i,:] = p(.... y^1_{ji} = 0), p(.... y^1_{ji} = 1)]
        
        # check at:
        assert( lp == lp0) # med usikkerhed 10^-10
    
    p_y[i] = exp(lp0[i,1]) / sum(exp(lp0[i,:]) )
```

# Uge 5
 - Implementer generativ model (beta^1, y^1, beta^2, y^2) der, givet X, generer datasættet. Bemærk: ingen w
 - ligning 12
 - Datastruktur
```python
X = N x M # Input data X
y^(2)_1 = N x 1 # dimensionelt

for k=1:M1:
    beta_k^1 # M x 1 dimensionel
    w^1_k # N x M
    y^1_k # N x 1
    
beta_1^2 # M1 x 1 dimensionel
w_1^2 # N x M1
y_1^2 # N x 1
```

```python
class NeuralPG():
    __init__(self, X, y, M1, ...)
        # konstruer (fx. ved random sampling, eller lignende y i det hidden layer)
        # dvs
        # y1[k] = N x M bin;r matrix for k in range M1
        
        self.layer1 = [LPG(X, y1[k]) for k in range(M1)]

        for k in range(M1):
            y_tmp[:,k] = self.layer1[k].y
        
        self.layer2 = [ LGP(y_tmp, y) ]
        
    # udregn log p(beta, y, w | X) for alle lag
    def logp(self):
        lp = 0
        for k in range(M1):
            lp += self.layer1[k].logp(...)
        lp += layer2[0].logp(...)
        return lp
        
    def mcmcsample():
        # foers sample beta, w med normal kode for lgp.
        # nu skal vi komme til y^1_k, som er N x 1 dimensionel.
        y1k = self.layer1[k].y
        # forestil dig at vi skal sample element nummer i
        
        for k in range(M1):
            for i in range(N):
                # Opdater datastruktur s[dan at self.layer1[k].y[i] = 0; husk kappa! husk lag2 (X!)
                lp0 = self.logp()
                # Opdater datastruktur s[dan at self.layer1[k].y[i] = 1
                lp1 = self.logp()
                theta = exp( lp1) / exp(lp0 + lp1) # underflow nogen gange.
                y_star = rand() < theta
                # opdater datastruktur (og husk kappe..husk X... alting)
                self.layer[k].y[i] = y_star
        
        # Opdater datastrukturen s[dan at]
        # logp( (alle parametre, men med y1k[i]= 0
        if self.do_posterior_predictive_checking:
            self.layer2[0].y = np.random.rand() < selflayer2[0].theta # <- theta skal udregnes som normalt, dvs. y^t * beta
```

# Uge 4
 - Få overblik over udledning af log p(y,beta,w|X) (log likelihood) og skriv den op i  LaTeX
 - Opdater LGP-klassen med ovenstaaende funktion, funktion til at sample lokale parametre (beta, w) givet X, y (i.e. samme metode som blev brugt til logitisk regression)
 - Taenk over hvordan y kan gibbs-samples (i.e., hvad skal udregnes), isaer hvis vi havde en generel funktion log p(y, ....|x)
 - proev at finde hoved og hale i tegningen over det store neurale netvaerk. Fx.: Hvor mange beta-parametre er der? hvor mange y-vaerdier? hvad er y og X i de forskellige lag?

# Uge 3
 - Pak Gibbs sampler (MV) ned i en LPG-klasse som indeholder en MCMC og logp-funktion, samt de noedvendige variable (X, y, beta, w)
 - Konstruer en PGNetwork-klasse med MCMC og logp-funktion; taenk over at den skal indeholde et netvaerk (fx. en array af arrays af LPG-instancer)
 - Udled (I LaTeX) formler for logp(y, beta, w|X) for LPG (dvs. LPG.logp)


# Uge 2
 - `my_gibbs` -> `logreg_univariate(X, y, (X_test=None, y_test = None), Tsamples=1000, b = None, sigma_B = 1, ppc=False, (n_sample_iter=5))`
 - make ws into a vector (w).
 - gem hver `n_sample_iter` parameterv;rdi. e.g. linie 69: `if (i+1) % n_sample_iter == 0: (save value to beta history)`
 - fx. linie 88: if X_test is not None: (beregn prediction af X_test udfra nuvaerende beta-sample og gem som `yhat[i] = X_test * beta` (test). 
 - y_hat_middel = np.mean( np.concatenate(yhat, axis=1), axis=1)
 - y_hat_middel er nu Ntest x 1
 - Mulighed: Tag Yhat matrix, udregn middelvaerdi y_hat_middel over akse 2, udregn AUC(y_test, y_hat_middel). 
 - Derefter: slaa PPC til igen og check!
 - Derefter: multivariate case (koden boer vaere saa ens som muligt). For multivarate, proev at begynde at implementere X_test, y_test. 

# Uge 1
 - Skift notation til at vaere konsistent med artikkel (B --> Vw, omaga -> w, etc.)
 - Bliv i 1d case som nu, med fokus paa posterior-predictive checking. Dvs. indfoer en if/else case: `if PPC: (...)` hvor y samples som vi talte om.
 - Lav histogram over beta med PPC; check at fordelingen svarer til prior. Proev at aendre prior og lav nye plots
 - Naar det virker, pak din funktion ind i en enkel metode med signatur i retning af:
 
```python
def PGlogreg(X, y, Tsamples=1000, PPC=False, Bprior=1):
   # implementering
```
Hvis PPC er true kan den evt. lave et histogram-plot.
 - Start et LaTeX dokument med de vigtigste formler (dvs. de fire du har implementeret plus en meget kort beskrivelse)
