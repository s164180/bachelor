# README
This is a repository with relevant code for the project "Exact inference of neural network type models using Pólya-Gamma stochastic vriables".

### Relevant code for logistic regression Gibbs sampler, multinomial logistic regression Gibbs sampler and SNN
- mvgibbs.py (code for logistic regression Gibbs sampler)
- multinomial.py (code for multinomial logistic regression Gibbs sampler)
- NN.py (code for SNN)

The following code is used for testing on the DTU compute cluster. 

### Relevant code for performing tests
- testsquare.py (test of synthettic data)
- testfischer.py (test of Fischer Iris)
- testbreast.py (breast cancer)
- testcryo.py (cryotherapy)
- testparkinsons.py (parkinsons)
- ANN.py (test ANN on all data sets)
- logistic.py (test logistic regression on all data sets)
- AUCLLplots.py (make AUC and LL plots)
- tests.py (performing t-test)
- plots.py (make traceplot of Beta and Lambda)

### Relevant helper functions to test on the DTU compute cluster

- init.py (Generates and .sh file and sends it to the cluster)
- start.py (Calls init.py many times with different settings of SNN)
- folder.py (code used to generate relevant folders)
- move.py (code used to move results to relevant folders)




# Old README

## Sanity check of Polya-Gamma sampling
Purpose is to check we know what the parameters does and that the sampler actually works.

First: Select a "standard notation" for the parameters in the Polya-Gamma distribution. Write this in a latex document.
Implement density of Polya-Gamma distribution, i.e. 

```
def log_polya_gamma(x, ...): # same parameter names as LaTeX
	pass
```

Next, for a few different parameter values, sample a *lot* of PG random samples `x_1, x_2, .... ~ PG(...)` <- python library code

Make a standard histogram; when appropriately normalized, the histogram should agree with the log_polya_gamma(x, ...) density.
(correct normalization means integral is 1. I.e. if dx is width of histogram bins, then \sum_i heigh_of_bin_i * dx = 1 )


## Future plan

 - Check we have a working PG random generator
 - Implement PG sampler for logistic regression
 - Neural networks
